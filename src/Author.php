<?php

namespace App;

use Symfony\Component\Validator\Constraints as Assert;

class Author
{
    #[Assert\Length(min: 10)]
    private string $name;
    #[Assert\Choice(choices: ['fiction', 'non-fiction'])]
    private string $genre;
    public function __construct($name, $genre)
    {
        $this->name = $name;
        $this->genre = $genre;
    }

    public function __toString(): string
    {
        return "{Author: name: ".$this->name.", genre: ".$this->genre.'}';
    }
}