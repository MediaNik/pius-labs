<?php

namespace App;

class Comment
{
    public User $user;
    public string $text;

    /**
     * @param User $user
     * @param string $text
     */
    public function __construct(User $user, string $text)
    {
        $this->user = $user;
        $this->text = $text;
    }
}