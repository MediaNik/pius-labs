<?php

// src/Entity/Author.php
namespace App;
require __DIR__.'/../vendor/autoload.php';

use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Validation;

$validator = Validation::createValidatorBuilder()
    ->enableAnnotationMapping()
    ->getValidator();

$violations = $validator->validate('Bernharfdgfdgd', [
    new Length(['min' => 10]),
    new NotBlank(),
]);

if (0 !== count($violations)) {
    // there are errors, now you can show them
    foreach ($violations as $violation) {
        echo $violation->getMessage().'<br>';
    }
}else {
    echo "Value 'Bernharfdgfdgd' passes validation\n";
}

$author = new Author('', 'abr');
$violations = $validator->validate($author);
if (0 !== count($violations)) {
    foreach ($violations as $violation) {
        echo $violation->getMessage()."\n";
    }
}else{
    echo "Passes validation: ". $author ."\n";
}

