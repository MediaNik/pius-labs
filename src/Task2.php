<?php

namespace App;
require __DIR__ . '/../vendor/autoload.php';

use SplQueue;
use Symfony\Component\Validator\Validation;

$user1 = new User(1, 'Michael', 'stafilopokmail.ru', 'password123');
$user2 = new User(2, 'Simon', 'medianik5@mail.ru', 'password3');
$user3 = new User(3, 'Michele', 'michael@mail.ru', 'password5');


$users = new SplQueue();
$users->enqueue($user1);
$users->enqueue($user2);
$users->enqueue($user3);

$comments = new SplQueue();
$comments->enqueue(new Comment($user1, 'Comment from '.$user1->name));
$comments->enqueue(new Comment($user2, 'Comment from '.$user2->name));
$comments->enqueue(new Comment($user3, 'Comment from '.$user3->name));
echo "Enter date: ";
$datetime = strtotime(readline());
foreach ($comments as $comment) {
    if($comment->user->getDate() > $datetime){
        echo $comment->text . ' with date of creation ' . date('y-M-d', $comment->user->getDate()) ."\n";
    }
}
