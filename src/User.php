<?php

// src/Entity/Author.php
namespace App;
require __DIR__ . '/../vendor/autoload.php';

use Symfony\Component\Validator\Constraints as Assert;

class User
{
    #[Assert\NotNull]
    public int $id;
    #[Assert\NotNull]
    #[Assert\NotBlank]
    public string $name;
    #[Assert\NotNull]
    #[Assert\Email]
    public string $email;
    #[Assert\NotNull]
    #[Assert\Length(min: 8, max: 24)]
    public string $password;

    private int $date;

    public function __construct(int $id, string $name, string $email, string $password)
    {
        $this->id = $id;
        $this->name = $name;
        $this->email = $email;
        $this->password = $password;
        $this->date = time();
    }

    /**
     * @return int
     */
    public function getDate(): int
    {
        return $this->date;
    }

    public function __toString(): string
    {
        return "{User: id: ".$this->id.
            ", name: ".$this->name.
            ", email: ".$this->email.
            ", password: ".$this->password.
            ", date: ".$this->date."}";
    }
}
